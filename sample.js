
var KEYSIZE = 14;
var MARGIN = 5;
var BOARDSIZE = {
  width: KEYSIZE*6+MARGIN*7,
  height: KEYSIZE*4+MARGIN*5 + 40,
};

function key(){
  return square({width: KEYSIZE, height: KEYSIZE});
}

function keys() {
  var rv = [];
  for (var x = 0;x < 6;x++) {
    for (var y = 0;y < 4;y++) {
      if (x<2 && y==3) continue;
      rv.push(
        key()
        .translate({
          x: MARGIN + KEYSIZE*x + MARGIN*x,
          y: MARGIN + KEYSIZE*y + MARGIN*y,
        }))
    }
  }
  rv.push(
    key()
    .rotate(60)
    .translate({x: MARGIN+28, y: MARGIN+KEYSIZE*3+MARGIN*3+3}));
  rv.push(
    key()
    .rotate(30)
    .translate({x: MARGIN+9, y: MARGIN+KEYSIZE*3+MARGIN*3+17}));
  rv.push(
    key()
    .translate({x: MARGIN, y: MARGIN+KEYSIZE*5+MARGIN*5}));
  return union(rv);
}

function keyboard() {
  return union([
    square(BOARDSIZE),
    keys(),
  ])
}


function main() {
  return keyboard().translate({x: 10, y:10});
}
