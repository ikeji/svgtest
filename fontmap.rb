#!/usr/bin/env ruby
require 'json'

output_data = {}
converts = 0
errors = 0
STDIN.each do |l|
  next if l.start_with? "*"
  num = l[0..3].to_i(16)
  begin
    if num < 0x100
      chr = [num].pack("C").force_encoding("ISO-2022-JP").encode("UTF-8")
      output_data[chr] = num
    else
     chr = [0x1b, 0x24, 0x42, num].pack("CCCn").force_encoding("ISO-2022-JP").encode("UTF-8")
     output_data[chr] = num
    end
    converts+=1
  rescue
    # STDERR.puts "error at: #{num.to_s(16)}"
    errors += 1
  end
end

STDERR.puts "#{converts} converted. #{errors} errors found."
puts "fontLoadCallback(#{JSON.dump(output_data)});"
