function gear(numTeeth, circularPitch, pressureAngle, clearance) {
  // default values:
  if(arguments.length < 3) pressureAngle = 20;
  if(arguments.length < 4) clearance = 0;
  
  var addendum = circularPitch / Math.PI;
  var dedendum = addendum + clearance;
  
  // radiuses of the 4 circles:
  var pitchRadius = numTeeth * circularPitch / (2 * Math.PI);
  var baseRadius = pitchRadius * Math.cos(Math.PI * pressureAngle / 180);
  var outerRadius = pitchRadius + addendum;
  var rootRadius = pitchRadius - dedendum;

  var maxtanlength = Math.sqrt(outerRadius*outerRadius - baseRadius*baseRadius);
  var maxangle = maxtanlength / baseRadius;

  var tl_at_pitchcircle = Math.sqrt(pitchRadius*pitchRadius - baseRadius*baseRadius);
  var angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;
  var diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);
  var angularToothWidthAtBase = Math.PI / numTeeth + 2*diffangle;

  var resolution = 10;
  var points = [];
  points.push([Math.cos(0) * rootRadius, Math.sin(0) * rootRadius]);
  for(var i = 0; i <= resolution; i++)
  {
    // first side of the tooth:
    var angle = maxangle * i / resolution;
    var tanlength = angle * baseRadius;
    var radx = Math.cos(angle);
    var rady = Math.sin(angle);
    var tanx = rady;
    var tany = -radx;
    points[i+1] = [radx * baseRadius+tanx * tanlength, rady * baseRadius+tany * tanlength];

    radx = Math.cos(angularToothWidthAtBase - angle);
    rady = Math.sin(angularToothWidthAtBase - angle);
    tanx = -rady;
    tany = radx;
    points[2*resolution+2-i] = [radx * baseRadius+tanx * tanlength, rady * baseRadius+tany * tanlength];
  }
  points.push([Math.cos(angularToothWidthAtBase) * rootRadius, Math.sin(angularToothWidthAtBase) * rootRadius]);
  var angle = 2 * Math.PI/numTeeth;
  points.push([Math.cos(angle) * rootRadius, Math.sin(angle) * rootRadius]);
  var lines = [];
  for (var i = 0; i < points.length -1 ; i++) {
    var o = points[i];
    var d = points[i+1];
    lines.push(line({x1: o[0], y1: o[1], x2:d[0], y2:d[1]}));
  }
  var halfTooth = union(lines);
  var lineset = [];
  for(var i=0;i<numTeeth;i++) {
    var ang = i*360/numTeeth;
    lineset.push(halfTooth.rotate(ang));
  }

  return union(lineset);
}

function main() {
  return gear(10, 5, 20, 0).translate({x: 10, y:10}).scale(10);
}
