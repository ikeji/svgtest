all: jsise.js font_table.js

jsise.js:
	cd jsise; npm install
	$(MAKE) -C jsise
	cp jsise/jsise.js .

font_table.js: fontmap.rb ./KST32Bv3/KST32B.TXT
	cat KST32Bv3/KST32B.TXT|ruby fontmap.rb > font_table.js

serve:
	python -m SimpleHTTPServer 8000&
	xdg-open http://localhost:8000

clean:
	rm -f jsise.js font_table.js
	make -C jsise clean


